# [C9x3] Firefox Left Click to Scroll Userscript

Left Click to Scroll using Firefoxes implementation of the Middle Mouse Scroll. This Userscript might NOT work with Mozilla sites/Firefox webpages. For example, about:config.

Forked from https://github.com/StoyanDimitrov/middle-mouse-button-scroll

# Licensing and donation information:

https://c9x3.neocities.org/

# How to run this Userscript

- Get a userscript runner. For examlpe, Violentmonkey. 
- Create a new script and copy and paste the contents into the new userscript! 
- Save that script, exit and test the script out. 

# How to use

- Ctrl+Left click to select text on a webpage.
- Hold down Shift+Left click to scroll slowly and precisely. 
- Copy and paste the RAW contents of the Userscript into a new Userscript. I suggest using Violent Monkey (an awesome Userscript extension for Firefox) to use the Firefox Left Click to Scroll Userscript. 
- You WILL need to reload the webpage if you enable and or disable the Firefox Left Click to Scroll Userscript.
