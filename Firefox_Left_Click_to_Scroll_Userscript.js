// ==UserScript==
// @name        [C9x3] Firefox Left Click to Scroll Userscript
// @match       *://*/*
// @exclude     *c9x3.github.io/*
// @exclude     *jummbus.bitbucket.io/*
// @exclude     *beepbox.co/*
// @grant       none
// @version            0.5
// ProjectHomepage      https://c9x3.neocities.org/
// @downloadURL  		https://gitlab.com/c9x3/firefox-left-click-to-scroll-userscript/-/raw/main/Firefox_Left_Click_to_Scroll_Userscript.js
// @updateURL    		https://gitlab.com/c9x3/firefox-left-click-to-scroll-userscript/-/raw/main/Firefox_Left_Click_to_Scroll_Userscript.js
// ==/UserScript==

// Forked from https://github.com/StoyanDimitrov/middle-mouse-button-scroll

// Make a log in the console when the script starts. 

console.log('Firefox Left Click to Scroll UserScript has started!')

//

(function() {
  function scrollHandler() {

    function scrollRatio(event) {
      let ratio = .8

      if (event.shiftKey) {
        ratio = 12
      }

      return ratio
    }

    function scrollAmount(event) {
      const amount = {
        x: 0,
        y: 0,
      }
      const ratio = scrollRatio(event)

      // horizontal scroll exists
      if (window.scrollMaxX !== 0) {
        let reachedLeft = false
        let reachedRight = false
        const scrollingLeft = event.movementX < 0
        const scrollingRight = event.movementX > 0

        if (window.scrollX === 0) {
          reachedLeft = true
        }
        if (window.scrollX === window.scrollMaxX) {
          reachedRight = true
        }

        if ((! reachedLeft && scrollingLeft)
          || (! reachedRight && scrollingRight)
        ) {
          const documentWidth = Math.max(document.body.scrollWidth,
            document.body.offsetWidth,
            document.documentElement.clientWidth,
            document.documentElement.scrollWidth,
            document.documentElement.offsetWidth
          )

          amount.x = event.movementX * Math.max(1, Math.ceil(documentWidth / (window.innerWidth * ratio)))
        }
      }

      // vertical scroll exists
      if (window.scrollMaxY !== 0) {
        let reachedTop = false
        let reachedBottom = false
        const scrollingUp = event.movementY < 0
        const scrollingDown = event.movementY > 0

        if (window.scrollY === 0) {
          reachedTop = true
        }
        if (window.scrollY === window.scrollMaxY) {
          reachedBottom = true
        }

        if ((! reachedTop && scrollingUp)
          || (! reachedBottom && scrollingDown)
        ) {
          const documentHeight = Math.max(document.body.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.clientHeight,
            document.documentElement.scrollHeight,
            document.documentElement.offsetHeight
          )

          amount.y = event.movementY * Math.max(1, Math.ceil(documentHeight / (window.innerHeight * ratio)))
        }
      }

      return amount
    }

    return function(event) {
      const scrollBy = scrollAmount(event)

      if (scrollBy.x === 0
        && scrollBy.y === 0
      ) {
        return
      }

      window.scrollBy(scrollBy.x, scrollBy.y)
    }
  }

  const handleScroll = scrollHandler()

  function setCursor(cursor) {
    if (cursor) {
      return document.body.style.cursor = cursor
    }

    document.body.style.removeProperty('cursor')

    if (document.body.style.length === 0) {
      document.body.removeAttribute('style')
    }
  }

  let doubleClick = false
  let doubleClickTime = 500

  // if ctrlkey is pressed... 

  window.addEventListener('mousedown', (event) => {
    if (event.ctrlKey) {
      return
    }

    // with left button click...
    if (event.button !== 0) {
      return
    }

    // no scroll
    if (window.scrollMaxX === 0
      && window.scrollMaxY === 0
    ) {
      return
    }

    if (doubleClick === true) {
      return
    }
    doubleClick = true
    setTimeout(() => { doubleClick = false }, doubleClickTime);

    event.preventDefault()

    setCursor('move')
    window.addEventListener('mousemove', handleScroll, true)
  })

  window.addEventListener('mouseup', () => {
    setCursor()
    window.removeEventListener('mousemove', handleScroll, true)
  })
  
}())

